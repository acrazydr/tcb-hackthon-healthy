import { Image, Text, View } from '@tarojs/components'
import './style.less'
import CardIcon from '../../assets/card-icon.png'
import CardTipIcon from '../../assets/card-tip-icon.png'

type Props = {
  title: string
  desc: string
  onClick?: () => any
}

export function Card(props: Props) {
  return (
    <View onClick={props.onClick} className="card-container">
      <Image className="icon" src={CardIcon} />
      <View className="info">
        <Text className="title">{props.title}</Text>
        <Text className="desc">{props.desc}</Text>
      </View>
      <Image src={CardTipIcon} className="tip-icon" />
    </View>
  )
}

Card.defaultProps = {
  onClick: () => {},
} as Partial<Props>
