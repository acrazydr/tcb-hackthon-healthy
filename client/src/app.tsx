import Taro, { Component, Config } from '@tarojs/taro'
import 'taro-ui/dist/style/index.scss'
import './app.less'
import { Index } from './pages/index'

class App extends Component {
  config: Config = {
    pages: ['pages/index/index', 'pages/knowledge/index', 'pages/anonymous/index', 'pages/stat/index'],
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: '校园战疫',
      navigationBarTextStyle: 'black',
    },
    permission: {
      'scope.userLocation': {
        desc: '需要位置信息来检测个人健康风险',
      },
    },
    cloud: true,
    navigateToMiniProgramAppIdList: ['wx34b0738d0eef5f78', 'wxffc1051032845ffa', 'wxa2c453d902cdd452', 'wxc888b51893c07e7c', 'wx811d61e1f61155a4'],
    tabBar: {
      selectedColor: '#6190e8',
      color: '#999999',
      list: [
        {
          pagePath: 'pages/index/index',
          text: '健康上报',
          iconPath: './assets/上报信息-未选中.png',
          selectedIconPath: './assets/上报信息.png',
        },
        {
          pagePath: 'pages/stat/index',
          text: '统计',
          iconPath: './assets/统计-未选中.png',
          selectedIconPath: './assets/统计.png',
        },
      ],
    },
  }

  componentDidMount() {
    if (process.env.TARO_ENV === 'weapp') {
      Taro.cloud.init()
    }
  }

  // 在 App 类中的 render() 函数没有实际作用
  // 请勿修改此函数
  render() {
    return <Index />
  }
}

Taro.render(
  <App />,
  document.getElementById('app')
)
