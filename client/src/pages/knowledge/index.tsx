import { View } from '@tarojs/components'
import { Config } from '@tarojs/taro'
import './index.less'
import { Card } from '../../components/Card/Card'

type Link = {
  title: string
  desc: string
  path?: string
  appId?: string
}

const links: Link[] = [
  {
    title: '防疫小妙招',
    desc: '快来看看有什么防疫小妙招吧',
    path: 'pages/webview/index.html?url=https%253A%252F%252F3g.dxy.cn%252Fnewh5%252Fview%252Fpneumonia',
    appId: 'wxffc1051032845ffa'
  },
  {
    title: '健康咨询',
    desc: '在线咨询三甲医生',
    path: 'pages/webview/index.html?url=https%253A%252F%252Fask.dxy.com%252Fview%252Fi%252Fvolunteer_care%252Fdisease&name=%E8%82%BA%E7%82%8E%E5%92%A8%E8%AF%A2',
    appId: 'wxffc1051032845ffa'
  },
  {
    title: '腾讯课堂',
    desc: '在家也可以继续学习',
    appId: 'wxa2c453d902cdd452',
    path: 'pages/index/index.html'
  }
]

export function Knowledge() {

  const toMiniProgram = (appId: string, path: string) => {
    Taro.navigateToMiniProgram({
      appId,
      path
    })
  }

  return (
    <View className="container">
      {links.map((link) => (
        <Card onClick={() => toMiniProgram(link.appId, link.path)} key={link.title} title={link.title} desc={link.desc} />
      ))}
    </View>
  )
}

Knowledge.config = {
  navigationBarTitleText: '校园防疫知识合集',
} as Config
